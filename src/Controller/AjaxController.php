<?php

namespace App\Controller;

use App\Repository\FeedbackRepository;
use App\Repository\OrdersRepository;
use App\Repository\SubscribesRepository;
use App\Repository\UsersRepository;
use App\Services\Images\ImagesService;
use App\Services\Mailer\Configuration\ConfigurationBuilder;
use App\Services\Mailer\MailerService;
use App\Services\Url\UrlService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class AjaxController extends AbstractController
{
    private const KEY_STATUS = 'status';

    private const STATUS_SUCCESS = 'success';
    private const STATUS_ERROR = 'error';

    /** @var Request */
    private $request;

    /** @var ParameterBagInterface */
    private $params;

    /**
     * AjaxController constructor.
     * @param RequestStack $request
     * @param ParameterBagInterface $params
     */
    public function __construct(RequestStack $request, ParameterBagInterface $params)
    {
        $this->request = $request->getCurrentRequest();
        $this->params = $params;
    }

    /**
     * @param UsersRepository $usersRepository
     * @param OrdersRepository $ordersRepository
     * @return JsonResponse
     */
    public function order(
        UsersRepository $usersRepository,
        OrdersRepository $ordersRepository
    ): JsonResponse {
        $name = strip_tags($this->request->get('name'));
        $phone = strip_tags($this->request->get('phone'));
        $date = strip_tags($this->request->get('date'));

        try {
            $user = $usersRepository->getUserByPhone($phone);
            if ($user === null) {
                $userId = $usersRepository->setNewUser($name, '', $phone);
            } else {
                $userId = $user->getId();
            }
            $hash = md5($userId . $date);
            $ordersRepository->setOrder($userId, $date, $hash);

            $mailerConfigurationBuilder = new ConfigurationBuilder();
            $configuration = $mailerConfigurationBuilder->buildNoReply($this->params);

            $mailerService = new MailerService($configuration);

            $order = [
                'name' => $name,
                'phone' => $phone,
                'date' => $date
            ];

            $mailerAdmins = (array) $this->params->get('mailer_admins');
            foreach ($mailerAdmins as $adminMail => $adminPassword) {
                $message = (new Swift_Message('Новая запись'))
                    ->setFrom([$configuration->getUser() => 'Маникюр'])
                    ->setTo($adminMail)
                    ->setBody($this->renderView('emails/admin-order.html.twig', [
                        'order' => $order
                    ]), 'text/html');

                $mailerService->send($message);
            }
            $status = self::STATUS_SUCCESS;
        } catch (Exception $e) {
            $status = self::STATUS_ERROR;
        }

        return new JsonResponse([self::KEY_STATUS => $status]);
    }

    /**
     * @param FeedbackRepository $feedbackRepository
     * @return JsonResponse
     */
    public function feedback(FeedbackRepository $feedbackRepository): JsonResponse
    {
        $name = strip_tags($this->request->get('name'));
        $email = strip_tags($this->request->get('email'));
        $text = strip_tags($this->request->get('text'));

        try {
            $feedbackRepository->setFeedback($name, $email, $text);

            $mailerConfigurationBuilder = new ConfigurationBuilder();
            $configuration = $mailerConfigurationBuilder->buildNoReply($this->params);

            $mailerService = new MailerService($configuration);

            $feedback = [
                'name' => $name,
                'email' => $email,
                'text' => $text
            ];

            $mailerAdmins = (array) $this->params->get('mailer_admins');
            foreach ($mailerAdmins as $adminMail => $adminPassword) {
                $message = (new Swift_Message('Feedback'))
                    ->setFrom([$configuration->getUser() => 'Маникюр'])
                    ->setTo($adminMail)
                    ->setBody($this->renderView('emails/admin-feedback.html.twig', [
                        'feedback' => $feedback
                    ]), 'text/html');

                $mailerService->send($message);
            }
            $status = self::STATUS_SUCCESS;
        } catch (Exception $e) {
            $status = self::STATUS_ERROR;
        }

        return new JsonResponse([self::KEY_STATUS => $status]);
    }

    public function subscribe(SubscribesRepository $subscribeRepository):JsonResponse
    {
        $email = strip_tags($this->request->get('email'));

        try {
            $subscribeRepository->setSubscribe($email);

            $mailerConfigurationBuilder = new ConfigurationBuilder();
            $configuration = $mailerConfigurationBuilder->buildNoReply($this->params);

            $mailerService = new MailerService($configuration);

            $subscribe = [
                'email' => $email
            ];

            $mailerAdmins = (array) $this->params->get('mailer_admins');
            foreach ($mailerAdmins as $adminMail => $adminPassword) {
                $message = (new Swift_Message('Subscribe'))
                    ->setFrom([$configuration->getUser() => 'Маникюр'])
                    ->setTo($adminMail)
                    ->setBody($this->renderView('emails/admin-subscribe.html.twig', [
                        'subscribe' => $subscribe
                    ]), 'text/html');

                $mailerService->send($message);
            }
            $status = self::STATUS_SUCCESS;
        } catch (Exception $e) {
            $status = self::STATUS_ERROR;
        }

        return new JsonResponse([self::KEY_STATUS => $status]);
    }

    /**
     * @param ImagesService $imagesService
     * @param UrlService $urlService
     * @return JsonResponse
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function loadGalleryImages(
        ImagesService $imagesService,
        UrlService $urlService
    ): JsonResponse {
        $offset = (int) $this->request->get('offset');
        $showButtonLoadImages = true;

        $images = $imagesService->getImagesByFolderId(
            ImagesService::GALLERY_FOLDER_ID,
            GalleryController::LIMIT_BATCH_IMAGES,
            $offset
        );
        if ($imagesService->isIncludeLastImage($images)) {
            $showButtonLoadImages = false;
        }
        $imagesService->applyMediaSize($images, ImagesService::HEIGHTEN_SIZE);
        $groupedArrayImagesByYear = [];
        foreach ($images as $image) {
            $image->setOriginalPath($urlService->formatFullUrl($image->getOriginalPath()));
            $image->setMediaPath($urlService->formatFullUrl($image->getMediaPath()));
            $groupedArrayImagesByYear[$image->getYear()][] = $image->toArray();
        }
        krsort($groupedArrayImagesByYear);

        return new JsonResponse([
            'status' => 'success',
            'images' => $groupedArrayImagesByYear,
            'showButtonLoadImages' => $showButtonLoadImages,
            'offset' => $offset + GalleryController::LIMIT_BATCH_IMAGES
        ]);
    }
}
