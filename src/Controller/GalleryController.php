<?php

namespace App\Controller;

use App\Repository\ImagesRepository;
use App\Services\Images\ImagesService;
use App\Services\MetaProvider\MetaProviderInterface;
use App\Services\Url\UrlServiceInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class GalleryController extends AbstractController
{
    public const LIMIT_BATCH_IMAGES = 20;

    private Request $request;

    public function __construct(
        RequestStack $request,
        private ImagesService $imagesService,
        private ImagesRepository $imagesRepository,
        private UrlServiceInterface $urlService,
        private MetaProviderInterface $metaProvider
    ) {
        $this->request = $request->getCurrentRequest();
    }

    /**
     * @return Response
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function gallery(): Response
    {
        $meta = $this->metaProvider->provideMeta($this->request->getRequestUri());

        $showButtonLoadMore = true;
        $images = $this->imagesService->getImagesByFolderId(ImagesService::GALLERY_FOLDER_ID, self::LIMIT_BATCH_IMAGES);
        if ($this->imagesService->isIncludeLastImage($images)) {
            $showButtonLoadMore = false;
        }
        $galleryImages = $this->imagesService->applyMediaSize($images, ImagesService::HEIGHTEN_SIZE);
        $groupedImagesByYear = [];
        foreach ($galleryImages as $image) {
            $image->setOriginalPath($this->urlService->formatFullUrl($image->getOriginalPath()));
            $image->setMediaPath($this->urlService->formatFullUrl($image->getMediaPath()));
            $groupedImagesByYear[$image->getYear()][] = $image;
        }
        krsort($groupedImagesByYear);
        $allYears = $this->imagesRepository->getAllImageYears();
        $allYears = array_diff_key($allYears, $groupedImagesByYear);

        return $this->render('gallery.html.twig', [
            'images' => $groupedImagesByYear,
            'showButtonLoadMore' => $showButtonLoadMore,
            'meta' => $meta,
            'offset' => self::LIMIT_BATCH_IMAGES,
            'years' => $allYears
        ]);
    }
}
