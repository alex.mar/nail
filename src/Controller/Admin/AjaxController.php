<?php

namespace App\Controller\Admin;

use App\Entity\BlockElementAbout;
use App\Entity\BlockElementGallery;
use App\Entity\BlockElementPrices;
use App\Repository\BlockElementAboutRepository;
use App\Repository\BlockElementGalleryRepository;
use App\Repository\BlockElementPricesRepository;
use App\Repository\BlockElementServicesRepository;
use App\Repository\BlocksRepository;
use App\Repository\ImagesRepository;
use App\Repository\MetaRepository;
use App\Repository\OrdersRepository;
use App\Repository\OrderCommentRepository;
use App\Repository\OrderPaymentRepository;
use App\Repository\UsersRepository;
use App\Services\Images\Folder\Folder;
use App\Services\Images\ImagesService;
use App\Services\Images\ImagesUploadServiceInterface;
use App\Services\UrlData\AdminUrlData;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Liip\ImagineBundle\Binary\Loader\FileSystemLoader;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class AjaxController extends AbstractController
{
    /** @var Request */
    private $request;

    /**
     * AjaxController constructor.
     * @param RequestStack $request
     */
    public function __construct(RequestStack $request)
    {
        $this->request = $request->getCurrentRequest();
    }

    /**
     * @param ImagesService $imagesService
     * @return JsonResponse
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createImageFolder(ImagesService $imagesService): JsonResponse
    {
        $status = 'success';
        $message = '';

        $folderName = strip_tags($this->request->get('name'));

        $data = [];
        $folder = $imagesService->getImagesFolderByName($folderName);
        if ($folder instanceof Folder) {
            $status = 'error';
            $message = 'Папка с таким именем уже существует';
        } else {
            $filesystem = new Filesystem();

            try {
                $urlData = new AdminUrlData($this->request);
                $folder = $imagesService->setImagesFolder(null, $folderName, 0, 1);
                $data['link'] = $urlData->getBaseUrl() . ImagesService::MAIN_IMAGE_FOLDER . '/' . $folder->getLink();
                $filesystem->mkdir(ImagesService::MAIN_IMAGE_FOLDER . DIRECTORY_SEPARATOR . $folder->getName());
            } catch (IOExceptionInterface $exception) {
                $status = 'error';
                $message = 'Ошибка при создании папки';
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data]);
    }

    public function loadMoreImages(ImagesService $imagesService): JsonResponse
    {
        $folderId = (int) $this->request->get('folderId');

        $images = $imagesService->getImagesByFolderId($folderId, ImagesController::LIMIT_BATCH_IMAGES);
        $result = [];
        foreach ($images as $image) {
            $result[] = $image->toArray();
        }

        return new JsonResponse(['data' => $result]);
    }

    public function createMeta(MetaRepository $metaRepository): JsonResponse
    {
        $status = 'success';
        $message = '';
        $data = [];

        $pageName = strip_tags($this->request->get('name'));
        $urlId = (int) $this->request->get('urlId');

        try {
            $metaId = $metaRepository->createNewMeta($urlId, $pageName);
            $data['link'] = $this->generateUrl('admin_seo') . '?' . SeoController::URL_KEY_META_ID . '=' . $metaId;
            $data['metaId'] = $metaId;
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при создании страницы';
        }

        return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data]);
    }

    public function saveMeta(MetaRepository $metaRepository): JsonResponse
    {
        $status = 'success';
        $message = '';
        $metaData = $this->request->get('metaData');
        try {
            $meta = $metaRepository->find($metaData['id']);
            if ($meta !== null) {
                $meta->setDisplayName($metaData['displayName']);
                $meta->setTitle($metaData['title']);
                $meta->setKeywords($metaData['keywords']);
                $meta->setDescription($metaData['description']);
                $metaRepository->setMeta($meta);
                $message = 'Meta данные успешно обновлены';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при обновлении meta данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    /**
     * @param ImagesRepository $imagesRepository
     * @return JsonResponse
     */
    public function saveAltImages(ImagesRepository $imagesRepository): JsonResponse
    {
        $altData = $this->request->get('altData');

        try {
            $imagesRepository->updateAltByImageIds($altData);
            $message = 'Alt успешно сохранен';
            $status = 'success';
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при обновлении alt данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function deleteImage(ImagesRepository $imagesRepository, ImagesService $imagesService): JsonResponse
    {
        $imageId = $this->request->get('imageId');

        try {
            $image = $imagesRepository->find($imageId);
            if ($image !== null) {
                $path = $image->getPath();
                $mediumPath = ltrim($imagesService->getMediaImagePath($path, ImagesService::MEDIUM_SIZE), '/');
                $smallPath = ltrim($imagesService->getMediaImagePath($path, ImagesService::SMALL_SIZE), '/');
                $heightenPath = ltrim($imagesService->getMediaImagePath($path, ImagesService::HEIGHTEN_SIZE), '/');

                $filesystem = new Filesystem();
                $filesystem->remove([$path, $mediumPath, $smallPath, $heightenPath]);

                $imagesRepository->deleteImageByIds([$imageId]);
                $message = 'Фото успешно удалено';
                $status = 'success';
            } else {
                $status = 'error';
                $message = 'Фото не найдено';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при удалении фото';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function serviceImagesAutocomplete(ImagesRepository $imagesRepository): JsonResponse
    {
        $query = $this->request->get('query');
        $images = $imagesRepository->searchImages($query);

        return new JsonResponse($images);
    }

    public function loadPopupImage(ImagesService $imagesService): JsonResponse
    {
        $path = $this->request->get('path');
        $popupPath = $imagesService->getMediaImagePath($path, ImagesService::SMALL_SIZE);
        return new JsonResponse([$popupPath]);
    }

    public function saveServicesBlock(
        BlocksRepository $blocksRepository,
        BlockElementServicesRepository $blockElementServicesRepository,
        ImagesRepository $imagesRepository
    ): JsonResponse {
        $blockData = $this->request->get('block');
        $blockId = (int) $blockData[BlocksRepository::FIELD_ID];

        try {
            $block = $blocksRepository->find($blockId);
            if ($block !== null) {
                if (isset($blockData[BlocksRepository::FIELD_TITLE])) {
                    $block->setTitle($blockData[BlocksRepository::FIELD_TITLE]);
                }
                if (isset($blockData[BlocksRepository::FIELD_DESCRIPTION])) {
                    $block->setDescription($blockData[BlocksRepository::FIELD_DESCRIPTION]);
                }
                $blocksRepository->updateBlock($block);
                $elementsData = $blockData['elements'];
                $elementsByElementId = [];
                $allElementsImages = [];
                foreach ($elementsData as $element) {
                    $elementsByElementId[$element[BlockElementServicesRepository::FIELD_ID]] = $element;
                    if (!empty($element[ImagesRepository::FIELD_PATH])) {
                        $allElementsImages[] = $element[ImagesRepository::FIELD_PATH];
                    }
                }
                $allElementsImages = array_unique($allElementsImages);
                $imagesPath = $imagesRepository->getImageIdsByPath($allElementsImages);
                $imageIdsByPath = [];
                foreach ($imagesPath as $item) {
                    $imageIdsByPath[$item[ImagesRepository::FIELD_PATH]] = $item[ImagesRepository::FIELD_ID];
                }
                $elements = $blockElementServicesRepository->findBy(
                    [BlockElementServicesRepository::FIELD_ID
                    => array_column($elementsData, BlockElementServicesRepository::FIELD_ID)]
                );
                foreach ($elements as $element) {
                    $elementId = $element->getId();
                    $elementData = $elementsByElementId[$elementId];
                    $element->setTitle($elementData[BlockElementServicesRepository::FIELD_TITLE]);
                    $element->setDescription($elementData[BlockElementServicesRepository::FIELD_DESCRIPTION]);
                    $element->setOrder($elementData[BlockElementServicesRepository::FIELD_ORDER]);
                    if (isset($imageIdsByPath[$elementData[ImagesRepository::FIELD_PATH]])) {
                        $element->setImageId($imageIdsByPath[$elementData[ImagesRepository::FIELD_PATH]]);
                    }
                }
                $blockElementServicesRepository->setElementEntities($elements);
                $message = 'Данные успешно обновлены';
                $status = 'success';
            } else {
                $status = 'error';
                $message = 'Блок не найден';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при сохранении данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function saveGalleryBlock(
        BlocksRepository $blocksRepository,
        BlockElementGalleryRepository $blockElementGalleryRepository,
        ImagesRepository $imagesRepository
    ): JsonResponse {
        $blockData = $this->request->get('block');
        $blockId = (int) $blockData[BlocksRepository::FIELD_ID];

        try {
            $block = $blocksRepository->find($blockId);
            if ($block !== null) {
                if (isset($blockData[BlocksRepository::FIELD_TITLE])) {
                    $block->setTitle($blockData[BlocksRepository::FIELD_TITLE]);
                }
                if (isset($blockData[BlocksRepository::FIELD_DESCRIPTION])) {
                    $block->setDescription($blockData[BlocksRepository::FIELD_DESCRIPTION]);
                }
                $blocksRepository->updateBlock($block);
                $elementsData = $blockData['elements'] ?? [];
                $existElementsIds = [];
                foreach ($elementsData as $index => $element) {
                    if (!empty($element[BlockElementGalleryRepository::FIELD_ID])) {
                        $elementId = $element[BlockElementGalleryRepository::FIELD_ID];
                        $existElementsIds[$elementId] = $elementId;
                    }
                }

                $existElementsByIds = [];
                if (!empty($existElementsIds)) {
                    $existElements = $blockElementGalleryRepository->findBy(
                        [BlockElementGalleryRepository::FIELD_ID => $existElementsIds]
                    );
                    $existElementsByIds = [];
                    foreach ($existElements as $element) {
                        $existElementsByIds[$element->getId()] = $element;
                    }
                }

                $toDelete = [];
                $allElementsGallery = $blockElementGalleryRepository->findAll();
                foreach ($allElementsGallery as $element) {
                    if (!isset($existElementsByIds[$element->getId()])) {
                        $toDelete[] = $element;
                    }
                }

                $allElementsImages = array_column($elementsData, ImagesRepository::FIELD_PATH);
                $allElementsImages = array_unique($allElementsImages);
                $imagesPath = $imagesRepository->getImageIdsByPath($allElementsImages);
                $imageIdsByPath = [];
                foreach ($imagesPath as $item) {
                    $imageIdsByPath[$item[ImagesRepository::FIELD_PATH]] = $item[ImagesRepository::FIELD_ID];
                }

                $toUpdate = [];
                foreach ($elementsData as $elementData) {
                    if (isset($imageIdsByPath[$elementData[ImagesRepository::FIELD_PATH]])) {
                        $entity = $existElementsByIds[$elementData[BlockElementGalleryRepository::FIELD_ID]]
                         ?? new BlockElementGallery();
                        $entity->setImageId($imageIdsByPath[$elementData[ImagesRepository::FIELD_PATH]]);
                        $entity->setOrder($elementData[BlockElementGalleryRepository::FIELD_ORDER]);
                        $toUpdate[] = $entity;
                    }
                }
                $blockElementGalleryRepository->setElementEntities($toUpdate);
                $blockElementGalleryRepository->removeEntities($toDelete);
                $message = 'Данные успешно обновлены';
                $status = 'success';
            } else {
                $status = 'error';
                $message = 'Блок не найден';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при сохранении данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function saveAboutBlock(
        BlocksRepository $blocksRepository,
        BlockElementAboutRepository $blockElementAboutRepository,
        ImagesRepository $imagesRepository
    ): JsonResponse {
        $blockData = $this->request->get('block');
        $blockId = (int) $blockData[BlocksRepository::FIELD_ID];
        $elementId = 0;

        try {
            $block = $blocksRepository->find($blockId);
            if ($block !== null) {
                if (isset($blockData[BlocksRepository::FIELD_TITLE])) {
                    $block->setTitle($blockData[BlocksRepository::FIELD_TITLE]);
                }
                if (isset($blockData[BlocksRepository::FIELD_DESCRIPTION])) {
                    $block->setDescription($blockData[BlocksRepository::FIELD_DESCRIPTION]);
                }
                $blocksRepository->updateBlock($block);
                $elementData = $blockData['element'] ?? [];
                if (!empty($elementData)) {
                    if (!empty($elementData[BlockElementAboutRepository::FIELD_ID])) {
                        $element = $blockElementAboutRepository->find(
                            $elementData[BlockElementAboutRepository::FIELD_ID]
                        );
                    } else {
                        $element = new BlockElementAbout();
                    }
                    if (isset($elementData[ImagesRepository::FIELD_PATH])) {
                        $path = $elementData[ImagesRepository::FIELD_PATH];
                        $imagesPath = $imagesRepository->getImageIdsByPath([$path]);
                        if (!empty($imagesPath)) {
                            $element->setImageId($imagesPath[0][ImagesRepository::FIELD_ID]);
                        }
                    }
                    $element->setTitle($elementData[BlockElementAboutRepository::FIELD_TITLE]);
                    $element->setSubtitle($elementData[BlockElementAboutRepository::FIELD_SUBTITLE]);
                    $element->setDescription($elementData[BlockElementAboutRepository::FIELD_DESCRIPTION]);
                    $elementId = $blockElementAboutRepository->setElement($element);
                }

                $message = 'Данные успешно обновлены';
                $status = 'success';
            } else {
                $status = 'error';
                $message = 'Блок не найден';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при сохранении данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message, 'data' => ['elementId' => $elementId]]);
    }

    public function savePricesBlock(
        BlocksRepository $blocksRepository,
        BlockElementPricesRepository $blockElementPricesRepository
    ): JsonResponse {
        $blockData = $this->request->get('block');
        $blockId = (int) $blockData[BlocksRepository::FIELD_ID];

        try {
            $block = $blocksRepository->find($blockId);
            if ($block !== null) {
                if (isset($blockData[BlocksRepository::FIELD_TITLE])) {
                    $block->setTitle($blockData[BlocksRepository::FIELD_TITLE]);
                }
                if (isset($blockData[BlocksRepository::FIELD_DESCRIPTION])) {
                    $block->setDescription($blockData[BlocksRepository::FIELD_DESCRIPTION]);
                }
                $blocksRepository->updateBlock($block);
                $elementsData = $blockData['elements'] ?? [];
                $existElementsIds = [];
                foreach ($elementsData as $index => $element) {
                    if (!empty($element[BlockElementPricesRepository::FIELD_ID])) {
                        $elementId = $element[BlockElementPricesRepository::FIELD_ID];
                        $existElementsIds[$elementId] = $elementId;
                    }
                }

                $existElementsByIds = [];
                if (!empty($existElementsIds)) {
                    $existElements = $blockElementPricesRepository->findBy(
                        [BlockElementPricesRepository::FIELD_ID => $existElementsIds]
                    );
                    $existElementsByIds = [];
                    foreach ($existElements as $element) {
                        $existElementsByIds[$element->getId()] = $element;
                    }
                }

                $toDelete = [];
                $allPrices = $blockElementPricesRepository->findAll();
                foreach ($allPrices as $price) {
                    if (!isset($existElementsByIds[$price->getId()])) {
                        $toDelete[] = $price;
                    }
                }

                $toUpdate = [];
                foreach ($elementsData as $elementData) {
                    $entity = $existElementsByIds[$elementData[BlockElementPricesRepository::FIELD_ID]]
                        ?? new BlockElementPrices();
                    $entity->setTitle($elementData[BlockElementPricesRepository::FIELD_TITLE]);
                    $entity->setPrice($elementData[BlockElementPricesRepository::FIELD_PRICE]);
                    $entity->setOrder($elementData[BlockElementPricesRepository::FIELD_ORDER]);
                    $toUpdate[] = $entity;
                }
                $blockElementPricesRepository->updateEntities($toUpdate);
                $blockElementPricesRepository->removeEntities($toDelete);
                $message = 'Данные успешно обновлены';
                $status = 'success';
            } else {
                $status = 'error';
                $message = 'Блок не найден';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при сохранении данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function saveRegistrationBlock(BlocksRepository $blocksRepository): JsonResponse
    {
        $blockData = $this->request->get('block');
        $blockId = (int) $blockData[BlocksRepository::FIELD_ID];

        try {
            $block = $blocksRepository->find($blockId);
            if ($block !== null) {
                if (isset($blockData[BlocksRepository::FIELD_TITLE])) {
                    $block->setTitle($blockData[BlocksRepository::FIELD_TITLE]);
                }
                if (isset($blockData[BlocksRepository::FIELD_DESCRIPTION])) {
                    $block->setDescription($blockData[BlocksRepository::FIELD_DESCRIPTION]);
                }
                $blocksRepository->updateBlock($block);

                $message = 'Данные успешно обновлены';
                $status = 'success';
            } else {
                $status = 'error';
                $message = 'Блок не найден';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при сохранении данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function saveContactsBlock(BlocksRepository $blocksRepository): JsonResponse
    {
        $blockData = $this->request->get('block');
        $blockId = (int) $blockData[BlocksRepository::FIELD_ID];

        try {
            $block = $blocksRepository->find($blockId);
            if ($block !== null) {
                if (isset($blockData[BlocksRepository::FIELD_TITLE])) {
                    $block->setTitle($blockData[BlocksRepository::FIELD_TITLE]);
                }
                if (isset($blockData[BlocksRepository::FIELD_DESCRIPTION])) {
                    $block->setDescription($blockData[BlocksRepository::FIELD_DESCRIPTION]);
                }
                $blocksRepository->updateBlock($block);

                $message = 'Данные успешно обновлены';
                $status = 'success';
            } else {
                $status = 'error';
                $message = 'Блок не найден';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при сохранении данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function saveImageData(
        ImagesRepository $imagesRepository
    ): JsonResponse {
        $imageData = $this->request->get('imageData');

        try {
            $id = (int) $imageData[ImagesRepository::FIELD_ID];
            $image = $imagesRepository->find($id);
            if ($image !== null) {
                $image->setAlt($imageData[ImagesRepository::FIELD_ALT]);
                $imagesRepository->updateEntity($image);

                $message = 'Данные успешно сохранены';
                $status = 'success';
            } else {
                $message = 'Картинка не найдена';
                $status = 'error';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при обновлении данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function rotateImage(
        ImagesRepository $imagesRepository,
        ImagesUploadServiceInterface $imagesUploadService,
        FileSystemLoader $fileSystemLoader,
        FilterManager $filterManager,
        ParameterBagInterface $params
    ): JsonResponse {
        ini_set('memory_limit', '512M');
        $imageId = (int) $this->request->get('imageId');
        $message = '';

        try {
            $image = $imagesRepository->find($imageId);
            if ($image !== null) {
                $imagePath = '/' . ltrim($image->getPath(), '/');
                $binary = $fileSystemLoader->find($imagePath);
                $fileSystem = new Filesystem();
                $filteredBinary = $filterManager->applyFilter($binary, 'rotate');
                $webDir = $params->get('kernel.project_dir');
                $fullPath = $webDir . '/public' . $imagePath;
                $fileSystem->dumpFile($fullPath, $filteredBinary->getContent());
                $imagesUploadService->uploadMediaImages([$image], true);

                $status = 'success';
            } else {
                $message = 'Картинка не найдена';
                $status = 'error';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при обновлении данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function deleteOrder(
        OrdersRepository $ordersRepository
    ): JsonResponse {
        $orderId = (int) $this->request->get('orderId');

        try {
            $order = $ordersRepository->find($orderId);
            if ($order !== null) {
                $ordersRepository->deleteOrderEntity($order);
                $message = 'Запись успешно удалена';
                $status = 'success';
            } else {
                $status = 'error';
                $message = 'Запись не найдена';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при удалении записи';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function editOrder(
        OrdersRepository $ordersRepository,
        OrderPaymentRepository $orderPaymentRepository,
        OrderCommentRepository $orderStatusCommentRepository
    ): JsonResponse {
        $orderId = (int) $this->request->get('orderId');
        $status = (string) $this->request->get('status');
        $payment = (int) $this->request->get('payment');
        $comment = (string) $this->request->get('comment');

        try {
            $ordersRepository->updateStatus($orderId, $status);
            if ($status === OrdersRepository::STATUS_DONE) {
                $orderPaymentRepository->setPayment($orderId, $payment);
            } elseif ($status === OrdersRepository::STATUS_CANCELED) {
                $orderStatusCommentRepository->setComment($orderId, $comment);
            }
            $message = 'Данные успешно обновлены';
            $status = 'success';
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при обновление данных';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function deleteUser(
        UsersRepository $usersRepository
    ): JsonResponse {
        $userId = (int) $this->request->get('userId');

        try {
            $user = $usersRepository->find($userId);
            if ($user !== null) {
                $usersRepository->deleteOrderEntity($user);
                $message = 'Клиент успешно удален';
                $status = 'success';
            } else {
                $status = 'error';
                $message = 'Клиент не найден';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при удалении клиента';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    public function createUser(
        UsersRepository $usersRepository
    ): JsonResponse {
        $name = $this->request->get('name');
        $surname = $this->request->get('surname');
        $phone = $this->request->get('phone');

        $data = [];

        try {
            $userEntity = $usersRepository->findOneBy(['phone' => $phone]);
            if ($userEntity === null) {
                $userId = $usersRepository->setNewUser($name, $surname, $phone);
                $data['userId'] = $userId;
                $data['link'] = $this->generateUrl('admin_user', ['userId' => $userId]);
                $message = 'Клиент успешно создан';
                $status = 'success';
            } else {
                $message = 'Клиент уже существует';
                $status = 'error';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при создании клиента';
        }

        return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data]);
    }

    public function createOrder(
        OrdersRepository $ordersRepository,
        UsersRepository $usersRepository
    ): JsonResponse {
        $userId = (int) $this->request->get('userId');
        $datetime = $this->request->get('datetime');
        $datetime = str_replace('T', ' ', $datetime);

        try {
            $userEntity = $usersRepository->find($userId);
            if ($userEntity !== null) {
                $hash = md5($userId . $datetime);
                $ordersRepository->setOrder($userId, $datetime, $hash);
                $status = 'success';
                $message = 'Запись успешно создана';
            } else {
                $message = 'Клиент не существует';
                $status = 'error';
            }
        } catch (Exception $exception) {
            $status = 'error';
            $message = 'Ошибка при создании клиента';
        }

        return new JsonResponse(['status' => $status, 'message' => $message]);
    }
}
