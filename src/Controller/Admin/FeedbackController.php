<?php

namespace App\Controller\Admin;

use App\Repository\FeedbackRepository;
use App\Services\Authorization\AuthorizationServiceInterface;
use App\Services\Menu\MenuServiceInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class FeedbackController extends BaseController
{
    /** @var FeedbackRepository */
    private $feedbackRepository;

    /**
     * SubscribesController constructor.
     * @param RequestStack $request
     * @param Environment $template
     * @param AuthorizationServiceInterface $authorizationService
     * @param MenuServiceInterface $menuService
     * @param FeedbackRepository $feedbackRepository
     */
    public function __construct(
        RequestStack $request,
        Environment $template,
        AuthorizationServiceInterface $authorizationService,
        MenuServiceInterface $menuService,
        FeedbackRepository $feedbackRepository
    ) {
        $this->feedbackRepository = $feedbackRepository;

        parent::__construct($request, $template, $authorizationService, $menuService);
    }

    public function feedback(): Response
    {
        $feedback = $this->feedbackRepository->findAll();
        return $this->render('admin/feedback/feedback.html.twig', [
            'feedback' => $feedback
        ]);
    }
}
