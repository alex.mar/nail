<?php

namespace App\Controller\Admin;

use App\Repository\BlocksRepository;
use App\Services\Authorization\AuthorizationServiceInterface;
use App\Services\Blocks\BlockItem\BlockItem;
use App\Services\Blocks\BlockServiceInterface;
use App\Services\Menu\MenuServiceInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class BlocksController extends BaseController
{
    /** @var BlocksRepository */
    private $blocksRepository;

    /** @var BlockServiceInterface */
    private $blockService;

    /**
     * BlocksController constructor.
     * @param RequestStack $request
     * @param Environment $template
     * @param AuthorizationServiceInterface $authorizationService
     * @param MenuServiceInterface $menuService
     * @param BlocksRepository $blocksRepository
     * @param BlockServiceInterface $blockService
     */
    public function __construct(
        RequestStack $request,
        Environment $template,
        AuthorizationServiceInterface $authorizationService,
        MenuServiceInterface $menuService,
        BlocksRepository $blocksRepository,
        BlockServiceInterface $blockService
    ) {
        $this->blocksRepository = $blocksRepository;
        $this->blockService = $blockService;

        parent::__construct($request, $template, $authorizationService, $menuService);
    }

    public function blocks(): Response
    {
        $blockId = (int) $this->request->get('blockId');

        $blockItem = null;

        if (!empty($blockId)) {
            $blockItem = new BlockItem($blockId);

            $block = $this->blocksRepository->find($blockId);
            if ($block !== null) {
                $blockItem->setName($block->getName())
                    ->setDisplayName($block->getDisplayName())
                    ->setTitle($block->getTitle() ?? '')
                    ->setDescription($block->getDescription() ?? '');

                $this->blockService->loadBlockContent($blockItem, true);
            }
        }

        $blocks = $this->blocksRepository->findAll();

        return $this->render('admin/blocks/blocks.html.twig', [
            'blocksMenu' => $blocks,
            'block' => $blockItem
        ]);
    }
}
