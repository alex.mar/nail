<?php

namespace App\Controller\Admin;

use App\Entity\Orders;
use App\Repository\OrderCommentRepository;
use App\Repository\OrderImageRelationsRepository;
use App\Repository\OrderPaymentRepository;
use App\Repository\OrdersRepository;
use App\Repository\UsersRepository;
use App\Services\Authorization\AuthorizationServiceInterface;
use App\Services\Images\ImagesService;
use App\Services\Images\ImagesUploadServiceInterface;
use App\Services\Menu\MenuServiceInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use RuntimeException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class OrdersController extends BaseController
{
    /** @var OrdersRepository */
    private $ordersRepository;

    /** @var UsersRepository */
    private $usersRepository;

    /** @var OrderPaymentRepository */
    private $orderPaymentRepository;

    /** @var OrderCommentRepository */
    private $orderCommentRepository;

    /** @var ImagesService */
    private $imagesService;

    /** @var ImagesUploadServiceInterface */
    private $imagesUploadService;

    /** @var OrderImageRelationsRepository */
    private $orderImageRelationsRepository;

    /**
     * OrdersController constructor.
     * @param RequestStack $request
     * @param Environment $template
     * @param AuthorizationServiceInterface $authorizationService
     * @param MenuServiceInterface $menuService
     * @param OrdersRepository $ordersRepository
     * @param UsersRepository $usersRepository
     * @param OrderPaymentRepository $orderPaymentRepository
     * @param OrderCommentRepository $orderCommentRepository
     * @param ImagesService $imagesService
     * @param ImagesUploadServiceInterface $imagesUploadService
     * @param OrderImageRelationsRepository $orderImageRelationsRepository
     */
    public function __construct(
        RequestStack $request,
        Environment $template,
        AuthorizationServiceInterface $authorizationService,
        MenuServiceInterface $menuService,
        OrdersRepository $ordersRepository,
        UsersRepository $usersRepository,
        OrderPaymentRepository $orderPaymentRepository,
        OrderCommentRepository $orderCommentRepository,
        ImagesService $imagesService,
        ImagesUploadServiceInterface $imagesUploadService,
        OrderImageRelationsRepository $orderImageRelationsRepository
    ) {
        $this->ordersRepository = $ordersRepository;
        $this->usersRepository = $usersRepository;
        $this->orderPaymentRepository = $orderPaymentRepository;
        $this->orderCommentRepository = $orderCommentRepository;
        $this->imagesService = $imagesService;
        $this->imagesUploadService = $imagesUploadService;
        $this->orderImageRelationsRepository = $orderImageRelationsRepository;

        parent::__construct($request, $template, $authorizationService, $menuService);
    }

    public function orders(): Response
    {
        $users = $this->usersRepository->findAll();
        $orders = $this->ordersRepository->getAllOrders();
        return $this->render('admin/orders/orders.html.twig', [
            'orders' => $orders,
            'users' => $users
        ]);
    }

    public function order(): Response
    {
        $orderId = (int) $this->request->get('orderId');
        $order = $this->ordersRepository->find($orderId);
        if ($order === null) {
            throw new RuntimeException('Order not found');
        }
        $user = $this->usersRepository->find($order->getUserId());
        if ($user === null) {
            throw new RuntimeException('User not found');
        }
        $orderId = $order->getId();
        $payment = $this->orderPaymentRepository->getPaymentByOrderId($orderId);
        $comment = $this->orderCommentRepository->getCommentByOrderId($orderId);

        $orderData = [
            'id' => $orderId,
            'created' => $order->getCreated()->format('Y-m-d'),
            'date' => $order->getDate(),
            'userName' => $user->getName(),
            'userPhone' => $user->getPhone(),
            'status' => $order->getStatus(),
            'payment' => $payment,
            'comment' => $comment
        ];
        return $this->render('admin/orders/order.html.twig', [
            'order' => $orderData
        ]);
    }

    public function orderReport(): Response
    {
        $hash = strip_tags(htmlspecialchars($this->request->get('hash')));
        /** @var Orders $order */
        $order = $this->ordersRepository->findOneBy(['hash' => $hash]);
        if ($order === null) {
            throw new RuntimeException('Order not found');
        }
        $user = $this->usersRepository->find($order->getUserId());
        if ($user === null) {
            throw new RuntimeException('User not found');
        }
        return $this->render('admin/orders/order-report.html.twig', [
            'order' => $order,
            'user' => $user
        ]);
    }

    /**
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function orderReportSave(): Response
    {
        $orderId = (int) $this->request->get('orderId');
        $status = $this->request->get('status');
        if (empty($status)) {
            return $this->redirectToRoute('index');
        }
        $this->ordersRepository->updateStatus($orderId, $status);
        $statusResponse = 'error';
        $message = 'Ошибка при сохранении данных';
        if ($status === OrdersRepository::STATUS_DONE) {
            $folder = $this->imagesService->getImagesFolderByName(ImagesService::FOLDER_IMAGES_GALLERY);
            if ($folder === null) {
                throw new RuntimeException('Images folder ' . ImagesService::FOLDER_IMAGES_GALLERY .' not found');
            }
            $payment = (int) $this->request->get('payment');
            $this->orderPaymentRepository->setPayment($orderId, $payment);
            /** @var UploadedFile $uploadImage */
            $uploadImage = $this->request->files->get('photo');
            $alt = $this->request->get('alt');
            $fullPath = $folder->getPath() . $uploadImage->getClientOriginalName();
            $this->imagesUploadService->uploadImage($fullPath, $alt, $folder->getId());
            $uploadImage->move($folder->getPath(), $uploadImage->getClientOriginalName());
            $uploadedImages = $this->imagesUploadService->send();
            $imageId = $uploadedImages[0]->getId();
            $this->orderImageRelationsRepository->setRelation($orderId, $imageId);
            $statusResponse = 'success';
            $message = 'Данные успешно сохранены';
        } elseif ($status === OrdersRepository::STATUS_CANCELED) {
            $comment = strip_tags($this->request->get('comment'));
            $this->orderCommentRepository->setComment($orderId, $comment);
            $statusResponse = 'success';
            $message = 'Данные успешно сохранены';
        }

        return $this->render('admin/orders/order-report-message.html.twig', [
            'status' => $statusResponse,
            'message' => $message
        ]);
    }
}
