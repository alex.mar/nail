<?php

namespace App\Controller\Admin;

use App\Services\Authorization\AdminAuthorizationService;
use App\Services\Authorization\AuthorizationServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LoginController extends AbstractController
{
    private const KEY_STATUS = 'status';
    private const KEY_MESSAGE = 'message';
    private const STATUS_ERROR = 'error';
    private const STATUS_SUCCESS = 'success';

    /** @var Request */
    private $request;

    /** @var SessionInterface */
    private $session;

    /** @var AuthorizationServiceInterface */
    private $authorizationService;

    /** @var ParameterBagInterface */
    private $params;

    /**
     * IndexController constructor.
     * @param RequestStack $request
     * @param SessionInterface $session
     * @param AuthorizationServiceInterface $authorizationService
     * @param ParameterBagInterface $params
     */
    public function __construct(
        RequestStack $request,
        SessionInterface $session,
        AuthorizationServiceInterface $authorizationService,
        ParameterBagInterface $params
    ) {
        $this->request = $request->getCurrentRequest();
        $this->session = $session;
        $this->authorizationService = $authorizationService;
        $this->params = $params;
    }

    public function login(): Response
    {
        $submit = $this->request->get('submit');
        if ($submit) {
            $message = '';
            $email = $this->request->get('email');
            $password = $this->request->get('password');

            $userId = $this->authorizationService->checkIssetUser($email, $password);
            if (!$userId) {
                $status = self::STATUS_ERROR;
                $message = 'Неверный логин или пароль!';
            } else {
                $status = self::STATUS_SUCCESS;
                $sessionKey = $this->authorizationService->generateSessionKey($userId);
                $this->session->set(AdminAuthorizationService::SESSION_USER_KEY, $sessionKey);
                $this->session->set(AdminAuthorizationService::SESSION_USER_ID, $userId);
            }
            if ($this->session->has('refererUrl')) {
                $url = $this->session->get('refererUrl');
                $this->session->remove('refererUrl');
            } else {
                $url = $this->params->get('url') . ltrim($this->generateUrl('admin_home'), '/');
            }

            return new JsonResponse([
                self::KEY_STATUS => $status,
                self::KEY_MESSAGE => $message,
                'url' => $url
            ]);
        }

        return $this->render('admin/login/login.html.twig');
    }

    /**
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        $this->session->clear();
        return $this->redirectToRoute('admin_login');
    }
}
