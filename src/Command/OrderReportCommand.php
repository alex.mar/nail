<?php

namespace App\Command;

use App\Repository\OrdersRepository;
use App\Services\Mailer\Configuration\ConfigurationBuilder;
use App\Services\Mailer\MailerService;
use Swift_Message;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class OrderReportCommand extends Command
{
    protected static $defaultName = 'app:order-report';

    /** @var ParameterBagInterface */
    private $params;

    /** @var Environment */
    private $twig;

    /** @var RouterInterface */
    private $router;

    /** @var OrdersRepository */
    private $ordersRepository;

    public function __construct(
        ParameterBagInterface $params,
        Environment $twig,
        RouterInterface $router,
        OrdersRepository $ordersRepository
    ) {
        $this->params = $params;
        $this->twig = $twig;
        $this->router = $router;
        $this->ordersRepository = $ordersRepository;

        parent::__construct();
    }

    protected function configure(): void
    {
        #CRON
        #018***cd/home/phpstore/domains/manic.od.ua && /usr/local/bin/php bin/console app:order-report
        $this
            ->setDescription('Order report')
            ->setHelp('This command send mail for order report...')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $orders = $this->ordersRepository->getOrdersByCurrentDate();
        if (!empty($orders)) {
            foreach ($orders as $order) {
                if ($order->getStatus() !== OrdersRepository::STATUS_WAITING) {
                    continue;
                }
                $url = $this->params->get('url');
                $link = $this->router->generate(
                    'admin_order_report',
                    ['hash' => $order->getHash()]
                );
                $fullUrl = $url . ltrim($link, '/');
                $mailerConfigurationBuilder = new ConfigurationBuilder();
                $configuration = $mailerConfigurationBuilder->buildNoReply($this->params);

                $mailerService = new MailerService($configuration);

                $mailerAdmins = (array) $this->params->get('mailer_admins');
                foreach ($mailerAdmins as $adminMail => $adminPassword) {
                    $message = (new Swift_Message('Отчет о заказе'))
                        ->setFrom([$configuration->getUser() => 'Маникюр'])
                        ->setTo($adminMail)
                        ->setBody($this->twig->render('emails/order-report.html.twig', [
                            'link' => $fullUrl
                        ]), 'text/html');

                    $mailerService->send($message);
                }
            }
        }

        return 0;
    }
}
