<?php

namespace App\Services\Images;

use App\Entity\Images;
use App\Repository\ImagesRepository;
use App\Repository\ImagesFolderRepository;
use App\Services\Images\Folder\Folder;
use App\Services\Images\Image\Image;
use App\Services\Translit\TranslitServiceInterface;
use App\Services\UrlData\UrlDataInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class ImagesService
{
    public const MAIN_IMAGE_FOLDER = 'images';
    public const MEDIA_IMAGE_FOLDER = 'media';
    public const SMALL_SIZE = '250';
    public const MEDIUM_SIZE = '500';
    public const HEIGHTEN_SIZE = '200';

    public const GALLERY_FOLDER_ID = 1;

    public const FOLDER_IMAGES_GALLERY = 'galereya';

    private array $mediaPathMapping = [
        self::SMALL_SIZE => 'small',
        self::MEDIUM_SIZE => 'medium',
        self::HEIGHTEN_SIZE => 'heighten'
    ];

    private ImagesFolderRepository $imagesFolderRepository;
    private ImagesRepository $imagesRepository;
    private TranslitServiceInterface $translitService;

    /**
     * ImagesService constructor.
     * @param ImagesFolderRepository $imagesFolderRepository
     * @param ImagesRepository $imagesRepository
     * @param TranslitServiceInterface $translitService
     */
    public function __construct(
        ImagesFolderRepository $imagesFolderRepository,
        ImagesRepository $imagesRepository,
        TranslitServiceInterface $translitService
    ) {
        $this->imagesFolderRepository = $imagesFolderRepository;
        $this->imagesRepository = $imagesRepository;
        $this->translitService = $translitService;
    }

    public function getAllImageFolders(UrlDataInterface $urlData): array
    {
        $imagesFolder = $this->imagesFolderRepository->findAll();
        if (empty($imagesFolder)) {
            return [];
        }

        $baseImagePath = $urlData->getBaseUrl() . self::MAIN_IMAGE_FOLDER;
        $folders = [];
        $notSelected = false;
        foreach ($imagesFolder as $folder) {
            $isActive = false;
            $link = $baseImagePath . '/' . $folder->getLink();
            $path = self::MAIN_IMAGE_FOLDER . '/' . $folder->getLink();
            if ($urlData->getCurrentUri() === $link) {
                $isActive = true;
                $notSelected = true;
            }
            $folders[] = new Folder(
                $folder->getId(),
                $folder->getName(),
                $folder->getDisplayName(),
                $link,
                $path,
                $isActive
            );
        }
        if ($notSelected === false) {
            $folders[0]->setIsActive(true);
        }

        return $folders;
    }

    /**
     * @param int|null $id
     * @param string $originalName
     * @param int $parentId
     * @param int $level
     * @return Folder
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setImagesFolder(?int $id, string $originalName, int $parentId, int $level): Folder
    {
        $folderName = $this->formatFolderName($originalName);
        $link = $folderName . '/';
        $path = self::MAIN_IMAGE_FOLDER . '/' . $folderName;

        $this->imagesFolderRepository->setImagesFolder($id, $folderName, $originalName, $link, $parentId, $level);

        return new Folder(null, $folderName, $originalName, $link, $path, false);
    }

    /**
     * @param string $folderName
     * @return Folder|null
     * @throws NonUniqueResultException
     */
    public function getImagesFolderByName(string $folderName): ?Folder
    {
        $folder = null;
        $folderName = $this->formatFolderName($folderName);
        $imagesFolder = $this->imagesFolderRepository->getImagesFolderByName($folderName);
        if ($imagesFolder !== null) {
            $folder = new Folder(
                $imagesFolder->getId(),
                $imagesFolder->getName(),
                $imagesFolder->getDisplayName(),
                $imagesFolder->getLink(),
                $path = self::MAIN_IMAGE_FOLDER . '/' .$imagesFolder->getLink(),
                false
            );
        }
        return $folder;
    }

    /**
     * @param int $folderId
     * @param int $limit
     * @param int $offset
     * @return Image[]
     */
    public function getImagesByFolderId(int $folderId, int $limit = 0, int $offset = 0): array
    {
        $imageEntities = $this->imagesRepository->getImagesByFolderId($folderId, $limit, $offset);
        return $this->buildImagesFromEntities($imageEntities);
    }

    /**
     * @param Images[] $imageEntities
     * @return Image[]
     */
    public function buildImagesFromEntities(array $imageEntities): array
    {
        $images = [];
        foreach ($imageEntities as $imageEntity) {
            $path = $imageEntity->getPath();
            $pathParts = explode('/', $path);
            $name = end($pathParts);
            $date = '';
            $year = (int) date('Y');
            $created = $imageEntity->getCreated();
            if ($created !== null) {
                $year = (int) $created->format('Y');
                $date = $created->format('d.m.Y');
            }
            $images[] = new Image(
                $imageEntity->getId(),
                $name,
                '/' . ltrim($path, '/'),
                '/' . ltrim($path, '/'),
                $imageEntity->getAlt(),
                0,
                $date,
                $year
            );
        }

        return $images;
    }

    private function formatFolderName(string $name): string
    {
        $folderName = $this->translitService->translit($name);
        return strtolower($folderName);
    }

    /**
     * @param string $imagePath
     * @param int $size
     * @return string
     */
    public function getMediaImageName(string $imagePath, int $size): string
    {
        $pathParts = explode('/', $imagePath);
        $imageName = end($pathParts);
        $nameParts = explode('.', $imageName);
        $cleanName = $nameParts[0];
        $smallName = $cleanName . '_' . $size;
        return $smallName . '.' . $nameParts[1];
    }

    /**
     * @param Image[] $images
     * @param int $size
     * @return Image[]
     */
    public function applyMediaSize(array $images, int $size): array
    {
        foreach ($images as $image) {
            $path = $image->getOriginalPath();
            $image->setMediaPath($this->getMediaImagePath($path, $size));
        }

        return $images;
    }

    public function getMediaImagePath(string $imagePath, int $size): string
    {
        $mediaName = $this->getMediaImageName($imagePath, $size);
        return '/' . self::MAIN_IMAGE_FOLDER
            . '/' . self::MEDIA_IMAGE_FOLDER
            . '/' . $this->mediaPathMapping[$size]
            . '/' . $mediaName;
    }

    public function calculateEmptyImages(int $countImages): int
    {
        $fullRows = $countImages % 4;
        $remainedCountImages = $countImages - 4 * $fullRows;
        return 4 - $remainedCountImages;
    }

    public function getCountAllImagesByFolderId(int $folderId): int
    {
        try {
            $count = $this->imagesRepository->getCountAllImagesByFolderId($folderId);
        } catch (NoResultException $e) {
            $count = 0;
        } catch (NonUniqueResultException $e) {
            $count = 0;
        }

        return $count;
    }

    public function getSizeByFilter(string $filter): int
    {
        $mapping = array_flip($this->mediaPathMapping);
        return $mapping[$filter];
    }

    public function getAllFilters(): array
    {
        return $this->mediaPathMapping;
    }

    /**
     * @param Image[] $images
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function isIncludeLastImage(array $images): bool
    {
        $isInclude = false;
        $lastImageId = $this->imagesRepository->getLastImageIdByFolderId(self::GALLERY_FOLDER_ID);
        $lastImage = end($images);
        $lastImageIdFromImages = $lastImage->getId();
        if ($lastImageId === $lastImageIdFromImages) {
            $isInclude = true;
        }

        return $isInclude;
    }
}
