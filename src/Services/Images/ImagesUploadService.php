<?php

namespace App\Services\Images;

use App\Entity\Images;
use App\Repository\ImagesRepository;
use App\Services\Images\Image\Image;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;

class ImagesUploadService implements ImagesUploadServiceInterface
{
    /** @var Images[] */
    private $imageEntities = [];

    /** @var ImagesRepository */
    private $imagesRepository;

    /** @var ImagesService */
    private $imagesService;

    /** @var CacheManager */
    private $cacheManager;

    /** @var DataManager */
    private $dataManager;

    /** @var FilterManager */
    private $filterManager;

    private $uploaded = 0;

    /**
     * ImagesUploadService constructor.
     * @param ImagesRepository $imagesRepository
     * @param ImagesService $imagesService
     * @param CacheManager $cacheManager
     * @param DataManager $dataManager
     * @param FilterManager $filterManager
     */
    public function __construct(
        ImagesRepository $imagesRepository,
        ImagesService $imagesService,
        CacheManager $cacheManager,
        DataManager $dataManager,
        FilterManager $filterManager
    ) {
        $this->imagesRepository = $imagesRepository;
        $this->imagesService = $imagesService;
        $this->cacheManager = $cacheManager;
        $this->dataManager = $dataManager;
        $this->filterManager = $filterManager;
    }

    /**
     * @param string $path
     * @param string $alt
     * @param int $folderId
     * @throws \Exception
     */
    public function uploadImage(string $path, string $alt, int $folderId): void
    {
        $image = new Images();
        $image->setPath($path);
        $image->setAlt($alt);
        $image->setFolderId($folderId);

        $this->imageEntities[] = $image;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function send(): array
    {
        $this->imagesRepository->setImages($this->imageEntities);
        $this->uploadMediaImages($this->imageEntities);

        return $this->imagesService->buildImagesFromEntities($this->imageEntities);
    }

    /**
     * @param Images[] $imageEntities
     * @param bool $forceSave
     * @return int
     */
    public function uploadMediaImages(array $imageEntities, bool $forceSave = false): int
    {
        $this->uploaded = 0;
        $images = $this->imagesService->buildImagesFromEntities($imageEntities);
        $filters = $this->imagesService->getAllFilters();
        foreach ($images as $image) {
            foreach ($filters as $filter) {
                $this->uploadByFilter($filter, $image, $forceSave);
            }
        }

        return $this->uploaded;
    }

    private function uploadByFilter(string $filter, Image $image, bool $forceSave = false): void
    {
        $path = '/' . ltrim($image->getOriginalPath(), '/');
        $size = $this->imagesService->getSizeByFilter($filter);
        $imageName = $this->imagesService->getMediaImageName($image->getOriginalPath(), $size);
        if ($forceSave || !$this->cacheManager->isStored($imageName, $filter)) {
            $binary = $this->dataManager->find($filter, $path);

            $filteredBinary = $this->filterManager->applyFilter($binary, $filter);
            $this->cacheManager->store($filteredBinary, $imageName, $filter);
            $this->uploaded++;
        }
    }
}
