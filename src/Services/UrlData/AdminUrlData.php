<?php

namespace App\Services\UrlData;

use Symfony\Component\HttpFoundation\Request;

class AdminUrlData implements UrlDataInterface
{
    /** @var Request */
    private $request;

    /**
     * AdminUrlData constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getBaseUrl(): string
    {
        return '/admin/';
    }

    public function getCurrentUri(): string
    {
        return $this->request->getRequestUri();
    }
}
