<?php

namespace App\Services\Menu;

use App\Repository\AdminMenuRepository;
use App\Services\Url\UrlService;
use Symfony\Component\HttpFoundation\Request;

class AdminMenuService implements MenuServiceInterface
{
    public const TWIG_KEY_TITLE = 'title';
    public const TWIG_KEY_LINK = 'link';
    public const TWIG_KEY_ICON = 'icon';
    public const TWIG_KEY_ACTIVE = 'active';

    /** @var AdminMenuRepository */
    private $adminMenuRepository;

    /**
     * AdminMenuService constructor.
     * @param AdminMenuRepository $adminMenuRepository
     */
    public function __construct(AdminMenuRepository $adminMenuRepository)
    {
        $this->adminMenuRepository = $adminMenuRepository;
    }

    public function loadMenu(Request $request): array
    {
        $result = [];
        $currentUrl = trim($request->getRequestUri(), '/');

        $menuItems = $this->adminMenuRepository->getAllMenuItems();
        foreach ($menuItems as $menuItem) {
            if ($menuItem->getActive() === AdminMenuRepository::VALUE_ACTIVE_NO) {
                continue;
            }
            $link = UrlService::formatUri($menuItem->getLink());
            $isActive = false;
            if (!$isActive && $currentUrl === trim($link, '/')) {
                $isActive = true;
            }
            $result[] = [
                self::TWIG_KEY_TITLE => $menuItem->getTitle(),
                self::TWIG_KEY_LINK => $link,
                self::TWIG_KEY_ICON => $menuItem->getIcon(),
                self::TWIG_KEY_ACTIVE => $isActive
            ];
        }

        return $result;
    }
}
