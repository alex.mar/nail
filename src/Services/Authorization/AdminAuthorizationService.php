<?php

namespace App\Services\Authorization;

use App\Repository\AdminUsersRepository;
use App\Repository\OrdersRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

class AdminAuthorizationService implements AuthorizationServiceInterface
{
    public const SESSION_USER_KEY = 'adminSessionUserKey';
    public const SESSION_USER_ID = 'adminSessionUserId';

    /** @var SessionInterface */
    private $session;

    /** @var RouterInterface */
    private $router;

    /** @var AdminUsersRepository */
    private $adminUsersRepository;

    public function __construct(
        SessionInterface $session,
        RouterInterface $router,
        AdminUsersRepository $adminUsersRepository
    ) {
        $this->session = $session;
        $this->router = $router;
        $this->adminUsersRepository = $adminUsersRepository;
    }

    public function checkAuth(Request $request): void
    {
        $sessionKey = (string) $this->session->get(self::SESSION_USER_KEY);
        $userId = (int) $this->session->get(self::SESSION_USER_ID);
        if (!$this->checkAuthUser($userId, $sessionKey)) {
            $refererUrl = $request->getSchemeAndHttpHost() . $request->getPathInfo();
            $this->session->set('refererUrl', $refererUrl);
            $redirect = new RedirectResponse($this->router->generate('admin_login'));
            $redirect->send();
            die();
        }
    }

    public function checkIssetUser(string $email, string $password): ?int
    {
        $password = md5($password);

        $userId = $this->adminUsersRepository->getIdByEmailAndPassword($email, $password);

        return $userId ?? null;
    }

    /**
     * @param int $userId
     * @return string
     */
    public function generateSessionKey(int $userId): string
    {
        $sessionKey = md5(uniqid(mt_rand(), true));
        $this->adminUsersRepository->updateSessionKeyByUserId($userId, $sessionKey);

        return  $sessionKey;
    }

    /**
     * @param mixed $userId
     * @param mixed $sessionKey
     * @return bool
     */
    private function checkAuthUser(int $userId, string $sessionKey): bool
    {
        if (empty($sessionKey) || empty($userId)) {
            return false;
        }

        $sessionKeyFromDB = $this->adminUsersRepository->getSessionKeyByUserId($userId);

        return !($sessionKey !== $sessionKeyFromDB);
    }
}
