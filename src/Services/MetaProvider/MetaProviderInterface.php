<?php

namespace App\Services\MetaProvider;

use App\Entity\Meta;

interface MetaProviderInterface
{
    public function provideMeta(string $url): Meta;
}
