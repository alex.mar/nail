<?php

namespace App\Services\MetaProvider;

use App\Entity\Meta;
use App\Repository\MetaRepository;
use App\Repository\UrlsRepository;
use App\Services\Url\UrlService;

class MetaProvider implements MetaProviderInterface
{
    /** @var UrlsRepository */
    private $urlsRepository;

    /** @var MetaRepository */
    private $metaRepository;

    /**
     * MetaProvider constructor.
     * @param UrlsRepository $urlsRepository
     * @param MetaRepository $metaRepository
     */
    public function __construct(UrlsRepository $urlsRepository, MetaRepository $metaRepository)
    {
        $this->urlsRepository = $urlsRepository;
        $this->metaRepository = $metaRepository;
    }

    public function provideMeta(string $url): Meta
    {
        $meta = null;
        $url = UrlService::formatTableUri($url);
        $urlId = $this->urlsRepository->getUrlIdByUrl($url);
        if (!empty($urlId)) {
            $meta = $this->metaRepository->getMetaByUrlId($urlId);
        }
        if ($meta === null) {
            $meta = $this->getDefaultMeta();
        }
        return $meta;
    }

    private function getDefaultMeta(): Meta
    {
        $meta = new Meta();
        $meta->setTitle('Маникюр в Одессе | Nail.od.ua');
        $meta->setKeywords('маникюр, маникюр одесса, мастер маникюра, маникюр на дому, услуги маникюра, nail');
        $meta->setDescription('Профессиональный маникюр в Одессе на дому. Классический, аппаратный, комбинированный маникюр. Только качественный материал. Все инструменты проходят стерилизацию. Сделаю красивый маникюр для Вас!');

        return $meta;
    }
}
