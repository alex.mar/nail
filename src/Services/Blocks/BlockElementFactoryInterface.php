<?php

namespace App\Services\Blocks;

use App\Services\Blocks\BlockElement\BlockElementInterface;

interface BlockElementFactoryInterface
{
    public function build(string $blockName): BlockElementInterface;
}
