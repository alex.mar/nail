<?php

namespace App\Services\Blocks;

use App\Services\Blocks\BlockItem\BlockItem;

interface BlockServiceInterface
{
    public function loadBlockContent(BlockItem $blockItem, bool $isAdmin = false): void;

    public function getAllBlocks(): array;
}
