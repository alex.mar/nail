<?php

namespace App\Services\Blocks\BlockElement;

use App\Repository\BlockElementServicesRepository;
use App\Services\Blocks\BlockItem\BlockItem;
use App\Services\Images\ImagesService;

class BlockElementServices implements BlockElementInterface
{
    private const KEY = 'services';

    /** @var BlockElementServicesRepository */
    private $blockElementServicesRepository;

    /** @var ImagesService */
    private $imagesService;

    /**
     * BlockElementServices constructor.
     * @param BlockElementServicesRepository $blockElementServicesRepository
     * @param ImagesService $imagesService
     */
    public function __construct(
        BlockElementServicesRepository $blockElementServicesRepository,
        ImagesService $imagesService
    ) {
        $this->blockElementServicesRepository = $blockElementServicesRepository;
        $this->imagesService = $imagesService;
    }

    /**
     * @param BlockItem $blockItem
     * @param bool $isAdmin
     */
    public function loadContent(BlockItem $blockItem, bool $isAdmin = false): void
    {
        $elements = $this->blockElementServicesRepository->getAllElements();
        foreach ($elements as $key => $element) {
            $mediaPath = '';
            if (!empty($element['image'])) {
                $mediaPath = $this->imagesService->getMediaImagePath(
                    $element['image'],
                    ImagesService::SMALL_SIZE
                );
            }
            $elements[$key]['imageMediaPath'] = $mediaPath;
        }
        $blockItem->setContent($elements);
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
