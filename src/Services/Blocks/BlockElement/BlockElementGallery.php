<?php

namespace App\Services\Blocks\BlockElement;

use App\Repository\BlockElementGalleryRepository;
use App\Repository\ImagesRepository;
use App\Services\Blocks\BlockItem\BlockItem;
use App\Services\Images\ImagesService;

class BlockElementGallery implements BlockElementInterface
{
    private const KEY = 'gallery';

    public const BLOCK_WORK_GALLERY_IMAGES_LIMIT = 12;
    public const WORK_GALLERY_GROUP_ITEM_LIMIT = 4;

    /** @var BlockElementGalleryRepository */
    private $blockElementGalleryRepository;

    /** @var ImagesService */
    private $imagesService;

    /** @var ImagesRepository */
    private $imagesRepository;

    /**
     * BlockElementGallery constructor.
     * @param BlockElementGalleryRepository $blockElementGalleryRepository
     * @param ImagesService $imagesService
     * @param ImagesRepository $imagesRepository
     */
    public function __construct(
        BlockElementGalleryRepository $blockElementGalleryRepository,
        ImagesService $imagesService,
        ImagesRepository $imagesRepository
    ) {
        $this->blockElementGalleryRepository = $blockElementGalleryRepository;
        $this->imagesService = $imagesService;
        $this->imagesRepository = $imagesRepository;
    }

    public function getKey(): string
    {
        return self::KEY;
    }

    public function loadContent(BlockItem $blockItem, bool $isAdmin = false): void
    {
        if ($isAdmin) {
            $content = $this->loadAdminContent();
        } else {
            $content = $this->loadFrontContent();
        }

        $blockItem->setContent($content);
    }

    private function loadFrontContent(): array
    {
        $customGalleryImages = $this->blockElementGalleryRepository->findAll();
        if (!empty($customGalleryImages)) {
            $customImageIds = [];
            foreach ($customGalleryImages as $image) {
                $imageId = $image->getImageId();
                $customImageIds[$imageId] = $imageId;
            }
            $imageEntities = $this->imagesRepository->findBy([ImagesRepository::FIELD_ID => $customImageIds]);
            $customImages = $this->imagesService->buildImagesFromEntities($imageEntities);
            $countDefaultImages = self::BLOCK_WORK_GALLERY_IMAGES_LIMIT - count($customImages);
            $defaultImageEntities = $this->imagesRepository->getImagesByFolderIdWithExcludeImages(
                ImagesService::GALLERY_FOLDER_ID,
                $countDefaultImages,
                $customImageIds
            );
            $defaultImages = $this->imagesService->buildImagesFromEntities($defaultImageEntities);
            $result = array_merge($customImages, $defaultImages);
        } else {
            $result = $this->imagesService->getImagesByFolderId(ImagesService::GALLERY_FOLDER_ID, 12);
        }

        $galleryImages = $this->imagesService->applyMediaSize($result, ImagesService::MEDIUM_SIZE);
        $modulo = count($galleryImages) % 4;
        $length = count($galleryImages) - $modulo;
        $galleryImages = array_slice($galleryImages, 0, $length);
        $groupedImages = $this->groupImages($galleryImages, self::WORK_GALLERY_GROUP_ITEM_LIMIT);

        return [
            'images' => $galleryImages,
            'groupedImages' => $groupedImages
        ];
    }

    private function loadAdminContent(): array
    {
        $customGalleryImages = $this->blockElementGalleryRepository->getAllGalleryImages();

        foreach ($customGalleryImages as $index => $image) {
            $customGalleryImages[$index]['media'] = $this->imagesService->getMediaImagePath(
                $image[ImagesRepository::FIELD_PATH],
                ImagesService::SMALL_SIZE
            );
        }

        return $customGalleryImages;
    }

    private function groupImages(array $images, int $limit): array
    {
        $result = [];

        $key = 0;
        foreach ($images as $image) {
            if (empty($result)) {
                $result[$key][] = $image;
                continue;
            }
            if (count($result[$key]) === $limit) {
                $key++;
            }
            $result[$key][] = $image;
        }

        return $result;
    }
}
