<?php

namespace App\Services\Blocks\BlockElement;

use App\Repository\BlockElementPricesRepository;
use App\Services\Blocks\BlockItem\BlockItem;

class BlockElementPrices implements BlockElementInterface
{
    private const KEY = 'prices';

    /** @var BlockElementPricesRepository */
    private $blockElementPricesRepository;

    /**
     * BlockElementPrices constructor.
     * @param BlockElementPricesRepository $blockElementPricesRepository
     */
    public function __construct(BlockElementPricesRepository $blockElementPricesRepository)
    {
        $this->blockElementPricesRepository = $blockElementPricesRepository;
    }

    public function getKey(): string
    {
        return self::KEY;
    }

    public function loadContent(BlockItem $blockItem, bool $isAdmin = false): void
    {
        $prices = $this->blockElementPricesRepository->getAllPrices();
        $blockItem->setContent($prices);
    }
}
