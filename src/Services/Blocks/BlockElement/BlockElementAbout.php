<?php

namespace App\Services\Blocks\BlockElement;

use App\Repository\BlockElementAboutRepository;
use App\Repository\ImagesRepository;
use App\Services\Blocks\BlockItem\BlockItem;
use App\Services\Images\ImagesService;

class BlockElementAbout implements BlockElementInterface
{
    private const KEY = 'about';

    /** @var BlockElementAboutRepository */
    private $blockElementAboutRepository;

    /** @var ImagesService */
    private $imagesService;

    /**
     * BlockElementAbout constructor.
     * @param BlockElementAboutRepository $blockElementAboutRepository
     * @param ImagesService $imagesService
     */
    public function __construct(
        BlockElementAboutRepository $blockElementAboutRepository,
        ImagesService $imagesService
    ) {
        $this->blockElementAboutRepository = $blockElementAboutRepository;
        $this->imagesService = $imagesService;
    }

    public function getKey(): string
    {
        return self::KEY;
    }

    public function loadContent(BlockItem $blockItem, bool $isAdmin = false): void
    {
        $element = $this->blockElementAboutRepository->getElement();
        if (!empty($element)) {
            $element = $element[0];
            $path = $element[ImagesRepository::FIELD_PATH];
            $path = $this->imagesService->getMediaImagePath($path, ImagesService::SMALL_SIZE);
            $element[ImagesRepository::FIELD_PATH] = $path;
            $blockItem->setContent($element);
        }
    }
}
