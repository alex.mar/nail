<?php

namespace App\Services\Blocks\BlockItem;

class BlockItem
{
    /** @var int */
    private $id;

    /** @var string */
    private $name = '';

    private $displayName = '';

    /** @var string */
    private $title = '';

    /** @var string */
    private $description = '';

    /** @var array */
    private $content = [];

    /**
     * BlockItem constructor.
     * @param int $id
     */
    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BlockItem
     */
    public function setName(string $name): BlockItem
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @param string $displayName
     * @return BlockItem
     */
    public function setDisplayName(string $displayName): BlockItem
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return BlockItem
     */
    public function setTitle(string $title): BlockItem
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return BlockItem
     */
    public function setDescription(string $description): BlockItem
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @param array $content
     * @return BlockItem
     */
    public function setContent(array $content): BlockItem
    {
        $this->content = $content;

        return $this;
    }
}
