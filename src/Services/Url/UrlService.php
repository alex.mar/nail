<?php

namespace App\Services\Url;

class UrlService implements UrlServiceInterface
{
    private string $baseUrl;

    public function __construct(
        string $baseUrl
    ) {
        $this->baseUrl = $baseUrl;
    }

    public static function formatUri(string $url): string
    {
        if (empty(trim($url, '/'))) {
            return $url;
        }
        return '/' . trim($url, '/') . '/';
    }

    public static function formatTableUri(string $url): string
    {
        return trim($url, '/') . '/';
    }

    public function formatFullUrl(string $url): string
    {
        $fullUrl = $url;
        if (!str_contains($url, $this->baseUrl)) {
            $fullUrl = $this->baseUrl . ltrim($url, '/');
        }
        return $fullUrl;
    }
}
