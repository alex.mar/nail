<?php

namespace App\Repository;

use App\Entity\Orders;
use App\Entity\Users;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Orders|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orders|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orders[]    findAll()
 * @method Orders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdersRepository extends ServiceEntityRepository
{
    public const STATUS_WAITING = 'waiting';
    public const STATUS_DONE = 'done';
    public const STATUS_CANCELED = 'canceled';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Orders::class);
    }

    public function getAllOrders(): array
    {
        return $this->createQueryBuilder('o')
            ->select('o.id', "DATE_FORMAT(o.date, '%Y-%m-%d %H:%i') as date", 'o.status', 'o.userId', 'u.name', 'u.surname', 'u.phone')
            ->join(Users::class, 'u', Join::WITH, 'u.id = o.userId')
            ->orderBy('o.date', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $userId
     * @param string $date
     * @param string $hash
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setOrder(int $userId, string $date, string $hash): void
    {
        $order = new Orders();
        $order->setUserId($userId);
        $order->setDate($date);
        $order->setStatus(self::STATUS_WAITING);
        $order->setHash($hash);

        $this->_em->persist($order);
        $this->_em->flush();
    }

    /**
     * @param Orders $order
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteOrderEntity(Orders $order): void
    {
        $this->_em->remove($order);
        $this->_em->flush();
    }

    public function updateStatus(int $orderId, string $status): void
    {
        $this->createQueryBuilder('o')
            ->update()
            ->set('o.status', ':status')
            ->where('o.id = :orderId')
            ->setParameters([
                'status' => $status,
                'orderId' => $orderId
            ])
            ->getQuery()
            ->execute();
    }

    /**
     * @param int $userId
     * @return int
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getCountOrdersByUserId(int $userId): int
    {
        $count = $this->createQueryBuilder('o')
            ->select('COUNT(o.id) as count')
            ->where('o.userId = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getSingleScalarResult();

        return (int) $count;
    }

    /**
     * @param int $userId
     * @return string
     */
    public function getLastOrderDateByUserId(int $userId): string
    {
        $date = '-';
        try {
            $date = $this->createQueryBuilder('o')
                ->select('o.date')
                ->where('o.userId = :userId')
                ->andWhere('o.date < CURRENT_DATE()')
                ->orderBy('o.date', 'DESC')
                ->setMaxResults(1)
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }
        return $date;
    }

    /**
     * @param int $userId
     * @return string
     */
    public function getNextOrderDateByUserId(int $userId): string
    {
        $date = '-';
        try {
            $date = $this->createQueryBuilder('o')
                ->select('o.date')
                ->where('o.userId = :userId')
                ->andWhere('o.date > CURRENT_DATE()')
                ->orderBy('o.date')
                ->setMaxResults(1)
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return $date;
    }

    public function getFirstOrderDateByUserId(int $userId): string
    {
        $date = '-';
        try {
            $date = $this->createQueryBuilder('o')
                ->select('o.date')
                ->where('o.userId = :userId')
                ->orderBy('o.date')
                ->setMaxResults(1)
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return $date;
    }

    /**
     * @return Orders[]
     */
    public function getOrdersByCurrentDate(): array
    {
        return $this->createQueryBuilder('o')
            ->select()
            ->where('DATE(o.date) = CURRENT_DATE()')
            ->getQuery()
            ->getResult();
    }
}
