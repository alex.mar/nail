<?php

namespace App\Repository;

use App\Entity\OrderImageRelations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderImageRelations|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderImageRelations|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderImageRelations[]    findAll()
 * @method OrderImageRelations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderImageRelationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderImageRelations::class);
    }

    public function setRelation(int $orderId, int $imageId): void
    {
        $relation = new OrderImageRelations();
        $relation->setOrderId($orderId);
        $relation->setImageId($imageId);

        $this->_em->persist($relation);
        $this->_em->flush();
    }
}
