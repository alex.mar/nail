<?php

namespace App\Repository;

use App\Entity\OrderPayment;
use App\Entity\Orders;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderPayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderPayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderPayment[]    findAll()
 * @method OrderPayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderPayment::class);
    }

    /**
     * @param int $orderId
     * @param int $payment
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setPayment(int $orderId, int $payment): void
    {
        $userPayment = new OrderPayment();
        $userPayment->setOrderId($orderId);
        $userPayment->setPayment($payment);

        $this->_em->persist($userPayment);
        $this->_em->flush();
    }

    /**
     * @param int $userId
     * @return int
     */
    public function getCountPaymentsByUserId(int $userId): int
    {
        $count = 0;
        try {
            $count = $this->createQueryBuilder('op')
                ->select('SUM(op.payment) as count')
                ->join(Orders::class, 'o', Join::WITH, 'o.id = op.orderId')
                ->where('o.userId = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return (int) $count;
    }

    public function getPaymentByOrderId(int $orderId): int
    {
        $payment = 0;
        try {
            $payment = $this->createQueryBuilder('op')
                ->select('op.payment')
                ->where('op.orderId = :orderId')
                ->setParameter('orderId', $orderId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return (int) $payment;
    }
}
