<?php

namespace App\Repository;

use App\Entity\AdminMenu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdminMenu|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminMenu|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminMenu[]    findAll()
 * @method AdminMenu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminMenuRepository extends ServiceEntityRepository
{
    public const VALUE_ACTIVE_YES = 'yes';
    public const VALUE_ACTIVE_NO = 'no';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminMenu::class);
    }

    /**
     * @return AdminMenu[]
     */
    public function getAllMenuItems(): array
    {
        return $this->createQueryBuilder('am')
            ->select()
            ->orderBy('am.order')
            ->getQuery()
            ->getResult();
    }
}
