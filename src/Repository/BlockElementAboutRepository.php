<?php

namespace App\Repository;

use App\Entity\BlockElementAbout;
use App\Entity\Images;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlockElementAbout|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlockElementAbout|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlockElementAbout[]    findAll()
 * @method BlockElementAbout[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlockElementAboutRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';
    public const FIELD_TITLE = 'title';
    public const FIELD_SUBTITLE = 'subtitle';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_IMAGE_ID = 'imageId';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlockElementAbout::class);
    }

    public function getElement(): array
    {
        return $this->createQueryBuilder('bea')
            ->select('bea.id', 'bea.title', 'bea.subtitle', 'bea.description', 'bea.imageId', 'i.path', 'i.alt')
            ->join(Images::class, 'i', Join::WITH, 'i.id = bea.imageId')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param BlockElementAbout $blockElementAbout
     * @return int
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setElement(BlockElementAbout $blockElementAbout): int
    {
        $this->_em->persist($blockElementAbout);
        $this->_em->flush();

        return $blockElementAbout->getId();
    }
}
