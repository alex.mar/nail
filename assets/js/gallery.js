import {loadingOff, loadingOn, STATUS_SUCCESS} from "./app";

import '../css/gallery/styles.css';
import '../css/gallery/media.css';

$(document).ready(function () {

    $('.gallery').justifiedGallery({
        lastRow : 'nojustify',
        rowHeight : 200,
        margins : 3,
        waitThumbnailsLoad: false
    }).on('jg.complete', function () {
        $(this).find('a').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            },
            navigateByImgClick: true
        });
    });

    $(window).scroll(function () {
        $('header').removeClass('default').fadeIn('fast');
    });

    $('.btn-load-gallery-images').on('click', function () {
        let btn = document.querySelector('.btn-load-gallery-images');
        let offset = btn.dataset.offset;

        loadingOn();
        $.ajax({
            url: '/gallery/action/load-images/',
            dataType: 'json',
            type: 'POST',
            data: {
                offset: offset
            },
            success: function (response) {
                if (response.status === STATUS_SUCCESS) {
                    btn.dataset.offset = response.offset;
                    $.each(response.images, function (year, groupedImages) {
                        let currentBlockGallery = document.querySelector(`.block-gallery[data-year='${year}']`);
                        if (currentBlockGallery.classList.contains('hidden')) {
                            currentBlockGallery.classList.remove('hidden');
                        }
                        $.each(groupedImages, function(index, image) {
                            let alt = image.alt;
                            let altCapitalized = alt.charAt(0).toUpperCase() + alt.slice(1);
                            $('.gallery[data-year="' + year + '"]').append('<a href="' + image.originalPath + '" data-image-id="' + image.id + '">\n' +
                                '                    <img alt="' + altCapitalized + '" src="' + image.mediaPath + '" height="200"/>\n' +
                                '                </a>');
                        });
                    });
                    $('.gallery').justifiedGallery('norewind');
                    if (response.showButtonLoadImages === false) {
                        $('.btn-load-gallery-images').hide();
                    }
                } else {
                    alert(response.message);
                }
                loadingOff();
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });

});