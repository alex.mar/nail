import '../app';

import '../../../css/admin/seo/styles.css';
import '../../../css/admin/seo/media.css';
import {loadingOff, loadingOn, validateForm, formatKeyValueArray} from "../app";

$(document).ready(function () {
    $('.btn-create-page').on('click', function (){
        if ($(this).hasClass('disabled')) {
            alert('Для всех url уже заданы meta данные. Создайте новый url чтобы создать новую страницу');
        } else {
            $.magnificPopup.open({
                items: {
                    src: '#popup-create-page',
                },
                closeBtnInside:true
            });
        }
    });

    $('.popup-btn-create-page').on('click', function () {
        let pageName = $('.page-name').val();
        let urlId = $('.selector-url-id option:selected').val();
        if (pageName.length === 0) {
            $('#popup-create-page form p.error').text('Введите название страницы');
            $('#popup-create-page form p.label').css('margin-bottom', '0');
            $('#popup-create-page form input').css('border', '1px solid #cc3636');
            return false;
        }
        loadingOn();
        $.ajax({
            url: '/admin/action/seo/create-meta/',
            dataType: 'json',
            type: 'POST',
            data: {
                name: pageName,
                urlId: urlId
            },
            success: function (response) {
                if (response.status === 'success') {
                    let data = response.data;
                    $('.pages-list ul').append('<li><a href="' + data.link + '"> <i class="fas fa-bars"></i> ' + pageName + ' </a></li>');
                    $('.mfp-close').trigger("click");
                } else if (response.status === 'error') {
                    $('#popup-create-page form p.error').text(response.message);
                    $('#popup-create-page form p.label').css('margin-bottom', '0');
                    $('#popup-create-page form input').css('border', '1px solid #cc3636');
                }
                loadingOff();
            },
            error: function () {
                loadingOff();
            }
        });

        return false;
    });

    $('.btn-save-meta').on('click', function () {
        let metaData = $('.form-meta:visible').serializeArray();

        if (validateForm(metaData) === false) {
            alert('Все поля должны быть заполнены!');
            return false;
        }

        metaData = formatKeyValueArray(metaData);
        metaData['id'] = $(this).data('meta-id');

        loadingOn();
        $.ajax({
            url: '/admin/action/seo/save/',
            dataType: 'json',
            type: 'POST',
            data: {
                metaData: metaData
            },
            success: function (response) {
                loadingOff();
                alert(response.message);
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });
});