const $ = require('jquery');
global.$ = global.jQuery = $;
global.$ = $;
require('mmenu-js');
require('mmenu-js/dist/mmenu.css');
require('mburger-css');
require('mburger-css/dist/mburger.css');
require('bootstrap/dist/js/bootstrap.min');
require('bootstrap/dist/css/bootstrap.css');
require('@fortawesome/fontawesome-free');
require('@fortawesome/fontawesome-free/css/all.css');
require('../../libs/js/modal-loading/modal-loading');
require('../../libs/css/modal-loading/modal-loading.css');
require('../../libs/css/modal-loading/modal-loading-animate.css');
require('magnific-popup');
require('magnific-popup/dist/magnific-popup.css');

import '../../css/admin/styles.css';
import '../../css/admin/media.css';

let loading;

export const STATUS_SUCCESS = 'success';
export const STATUS_ERROR = 'error';

export function getAllInputValues(inputItems)
{
    let result = [];
    $(inputItems).map(function () {
        let key = $(this).attr('name');
        result[key] = $(this).val();
    })
    return result;
}

export function loadingOn()
{
    loading = new Loading({
        loadingPadding: 8,
        loadingBorderRadius: 4,
        animationWidth: 35,
        animationHeight: 35
    });
}

export function loadingOff()
{
    loading.out();
}

export function validateForm(array)
{
    let bool = true;
    if (array.length === 0) {
        bool = false;
    } else {
        $.each(array, function (key, value) {
            if ($.trim(value).length === 0 || $.trim(value.value).length === 0) {
                bool = false;
            }
        });
    }

    return bool;
}

export function formatKeyValueArray(array)
{
    let result = {};

    $.each(array, function (key, value) {
        if (value.length === 0 || value.value.length === 0) {
            return true;
        }
        result[value.name] = value.value;
    });

    return result;
}

$(document).ready(function () {

    $('.btn-mmenu').on('click', function () {
        $('#main-menu').mmenu({
            "extensions": [
                "pagedim-black",
                "border-none"
            ]
        });
    });

});