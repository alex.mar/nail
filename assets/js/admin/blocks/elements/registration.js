import '../blocks';

import '../../../../css/admin/blocks/elements/registration/styles.css';
import '../../../../css/admin/blocks/elements/registration/media.css';

import {formatKeyValueArray, loadingOff, loadingOn} from "../../app";

$(document).ready(function () {

    $('.btn-save-block-registration').on('click', function () {
        let blockId = $(this).data('block-id');
        let headerData = $('.form-header-registration:visible').serializeArray();
        headerData = formatKeyValueArray(headerData);

        let blockData = {
            id: blockId,
            title: headerData['title'],
            description: headerData['description']
        };

        loadingOn();
        $.ajax({
            url: '/admin/action/blocks/registration/save/',
            dataType: 'json',
            type: 'POST',
            data: {
                block: blockData
            },
            success: function (response) {
                loadingOff();
                alert(response.message);
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });

});