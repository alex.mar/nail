import '../blocks';

require('easy-autocomplete/dist/jquery.easy-autocomplete.min');
require('easy-autocomplete/dist/easy-autocomplete.min.css');
require('jquery-ui-sortable-npm/jquery-ui-sortable.min');

import '../../../../css/admin/blocks/elements/gallery/styles.css';
import '../../../../css/admin/blocks/elements/gallery/media.css';

import {inputImageAutocomplete, catchPaste} from "../blocks";
import {formatKeyValueArray, getAllInputValues, loadingOff, loadingOn} from "../../app";

$(document).ready(function () {

    $('.btn-add-image').on('click', function () {
        let countImages = $('.form-elements-gallery table tr').length;
        if (countImages === 12) {
            alert('Добавлено максимальное количесвто фото!');
            return false;
        }
        $('.form-elements-gallery table').append('<tr>\n' +
            '                <td>\n' +
            '                    <div class="image-path-wrapper">\n' +
            '                        <input type="text" name="path" value="" data-image-id="1" class="input-image-autocomplete" id="test"/>\n' +
            '                    </div>\n' +
            '                </td>\n' +
            '                <td>\n' +
            '                    <div>\n' +
            '                        <img src="/public/images/no-image-icon.jpg" alt="test"/>\n' +
            '                    </div>\n' +
            '                </td>\n' +
            '                <td>\n' +
            '                    <div class="image-menu">\n' +
            '                        <p><i class="fas fa-trash btn-delete-image"></i></p>\n' +
            '                    </div>\n' +
            '                </td>\n' +
            '            </tr>');
    });

    $('.form-elements-gallery table')
    .on('click', '.btn-delete-image', function () {
        if (confirm('Вы уверены что хотите удалить фото?')) {
            $(this).closest('tr').remove();
        }

        return false;
    });

    $('.btn-save-block-gallery').on('click', function () {
        let blockId = $(this).data('block-id');
        let headerData = $('.form-header-gallery:visible').serializeArray();
        headerData = formatKeyValueArray(headerData);
        let elementsData = [];
        let order = 0;
        $('.form-elements-gallery table tr').each(function() {
            let elementId = $(this).data('element-id') ? $(this).data('element-id') : null;
            let inputItem = $(this).find('input');
            let elementData = getAllInputValues(inputItem);
            elementData['id'] = elementId;
            elementData['order'] = order;
            let myObject = Object.assign({}, elementData);
            elementsData.push(myObject);
            order++;
        });

        let blockData = {
            id: blockId,
            title: headerData['title'],
            description: headerData['description'],
            elements: elementsData
        };

        loadingOn();
        $.ajax({
            url: '/admin/action/blocks/gallery/save/',
            dataType: 'json',
            type: 'POST',
            data: {
                block: blockData
            },
            success: function (response) {
                loadingOff();
                alert(response.message);
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });

    $('.sortable-table tbody').sortable();

});