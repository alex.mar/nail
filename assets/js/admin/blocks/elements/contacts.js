import '../blocks';

import '../../../../css/admin/blocks/elements/contacts/styles.css';
import '../../../../css/admin/blocks/elements/contacts/media.css';

import {formatKeyValueArray, loadingOff, loadingOn} from "../../app";

$(document).ready(function () {

    $('.btn-save-block-contacts').on('click', function () {
        let blockId = $(this).data('block-id');
        let headerData = $('.form-header-contacts:visible').serializeArray();
        headerData = formatKeyValueArray(headerData);

        let blockData = {
            id: blockId,
            title: headerData['title'],
            description: headerData['description']
        };

        loadingOn();
        $.ajax({
            url: '/admin/action/blocks/contacts/save/',
            dataType: 'json',
            type: 'POST',
            data: {
                block: blockData
            },
            success: function (response) {
                loadingOff();
                alert(response.message);
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });

});