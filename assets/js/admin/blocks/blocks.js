import '../app';

require('easy-autocomplete/dist/jquery.easy-autocomplete.min');
require('easy-autocomplete/dist/easy-autocomplete.min.css');

import '../../../css/admin/blocks/styles.css';
import '../../../css/admin/blocks/media.css';

export function inputImageAutocomplete(input)
{
    input.easyAutocomplete({
        url: function(phrase) {
            return "/admin/action/blocks/images-autocomplete/?query=" + phrase;
        },

        getValue: function(element) {
            return element.path;
        },

        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },

        preparePostData: function(data) {
            data.phrase = $(this).val();
            return data;
        },

        requestDelay: 400
    });
}

export function catchPaste(evt, elem, callback) {
    if (navigator.clipboard && navigator.clipboard.readText) {
        // modern approach with Clipboard API
        navigator.clipboard.readText().then(callback);
    } else if (evt.originalEvent && evt.originalEvent.clipboardData) {
        // OriginalEvent is a property from jQuery, normalizing the event object
        callback(evt.originalEvent.clipboardData.getData('text'));
    } else if (evt.clipboardData) {
        // used in some browsers for clipboardData
        callback(evt.clipboardData.getData('text/plain'));
    } else if (window.clipboardData) {
        // Older clipboardData version for Internet Explorer only
        callback(window.clipboardData.getData('Text'));
    } else {
        // Last resort fallback, using a timer
        setTimeout(function() {
            callback(elem.value)
        }, 100);
    }
}

$(document).ready(function () {

    // inputImageAutocomplete($(".input-image-autocomplete"));

    $(".input-image-autocomplete").on('paste', function (evt) {
        catchPaste(evt, this, function(clipData) {
            $(this).val(clipData);
        });
    });

    $('.image-icon').on('click', function() {
        let path = $(this).parent('.image-path-wrapper').find('input').val();
        if (path.toLowerCase().indexOf('.jpg') >= 0 || path.toLowerCase().indexOf('.png') >= 0) {
            Tipped.create($(this),{
                position: 'topright',
                skin: 'light',
                radius: true,
                cache: false,
                showOn: false,
                ajax: {
                    url: '/admin/action/load-popup-image/',
                    type: 'post',
                    data: {
                        path: path
                    },
                    success: function(popupPath) {
                        return '<img src="' + popupPath + '" alt="image"/>';
                    }
                }
            });
            Tipped.show($(this));
        } else {
            Tipped.create($(this),'<span>image not found</span>', {
                position: 'topright',
                skin: 'light',
                radius: true,
                cache: false,
                showOn: false
            });
            Tipped.show($(this));
        }
    })
    .on('mouseover', function() {
        Tipped.hide('.image-icon');
    })
    .on('mouseenter', function() {
        Tipped.hide('.image-icon');
    })
    .on('mouseleave', function() {
        Tipped.hide('.image-icon');
    });
});