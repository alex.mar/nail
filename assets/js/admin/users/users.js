import '../app';

require('jquery.maskedinput/src/jquery.maskedinput');
require('jsmartable/jsmartable');

import '../../../css/admin/users/styles.css';
import '../../../css/admin/users/media.css';
import {loadingOff, loadingOn, validateForm, formatKeyValueArray, STATUS_SUCCESS} from "../app";

$(document).ready(function () {

    $('.btn-delete-user').on('click', function () {
        let userId = $(this).data('user-id');

        if (confirm('Вы уверены что хотите удалить клиента?')) {
            loadingOn();
            $.ajax({
                url: '/admin/action/users/delete-user/',
                dataType: 'json',
                type: 'POST',
                data: {
                    userId: userId
                },
                success: function (response) {
                    if (response.status === STATUS_SUCCESS) {
                        loadingOff();
                        alert(response.message);
                        window.location.href = window.location.origin + '/admin/users/';
                    }
                },
                error: function () {
                    loadingOff();
                    alert('Что то пошло не так! Попробуйте позже');
                }
            });
        }

        return false;
    });

    $('.btn-create-user').on('click', function () {
        $.magnificPopup.open({
            items: {
                src: '#popup-create-user',
            },
            closeBtnInside:true
        });
    });

    $('#user-phone').mask('+38 (099) 99-99-999', {
        placeholder:"+38 (0__) __-__-___",
        autoclear: false
    });

    $('.popup-btn-create-user').on('click', function () {
        let userName = $('.user-name').val();
        let userSurname = $('.user-surname').val();
        let userPhone = $('.user-phone').val();
        if (userName.length === 0 || userSurname.length === 0 || userPhone.length === 0) {
            $('#popup-create-user form p.error').text('Все поля должны быть заполнены');
            return false;
        } else {
            $('#popup-create-user form p.error').css('display', 'none');
        }
        loadingOn();
        $.ajax({
            url: '/admin/action/users/create-user/',
            dataType: 'json',
            type: 'POST',
            data: {
                name: userName,
                surname: userSurname,
                phone: userPhone,
            },
            success: function (response) {
                if (response.status === 'success') {
                    window.location.href = window.location.origin + '/admin/users/';
                } else if (response.status === 'error') {
                    $('#popup-create-user form p.error').text(response.message).css('display', 'block');
                }
                loadingOff();
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так. Попробуйте позже');
            }
        });

        return false;
    });

});